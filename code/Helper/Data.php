<?php

class Mgcs_RelAlternate_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_STORES = 'catalog/mgcs_relalternate/link_to_stores';
    const XML_PATH_CATEGORIES_ENABLED = 'catalog/mgcs_relalternate/categories_enabled';
    const XML_PATH_PRODUCTS_ENABLED = 'catalog/mgcs_relalternate/products_enabled';
    const XML_PATH_HREFLANG = 'catalog/mgcs_relalternate/hreflang';

    /**
     * Is the functionality enabled for products?
     *
     * @param Mage_Core_Model_Store|int $storeId
     * @return bool
     */
    public function isEnabledForProducts($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PRODUCTS_ENABLED, $storeId);
    }

    /**
     * Is the functionality enabled for categories?
     *
     * @param Mage_Core_Model_Store|int $storeId
     * @return bool
     */
    public function isEnabledForCategories($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_CATEGORIES_ENABLED, $storeId);
    }


    /**
     * Get Alternate Store Ids
     *
     * @param Mage_Core_Model_Store|int $storeId
     * @return array
     */
    public function getAlternateStoreIds($storeId = null)
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_STORES, $storeId));
    }

    /**
     * Get Hreflang value
     *
     * @param Mage_Core_Model_Store|int $storeId
     * @return string
     */
    public function getHreflang($storeId = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_HREFLANG, $storeId);
    }
}