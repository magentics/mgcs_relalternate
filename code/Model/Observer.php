<?php

class Mgcs_RelAlternate_Model_Observer
{

    /**
     * Add rel="alternate" to product view pages
     *
     * event: controller_action_layout_render_before_catalog_product_view
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function addRelForCatalogProductView(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Product */
        $product = Mage::registry('current_product');
        if (!$product) {
            return;
        }

        $currentUrl = $product->getProductUrl(false);
        $currentStoreId = Mage::app()->getStore()->getId();
        $helper = $this->_helper();

        if ($helper->isEnabledForProducts()) {
            $stores = $helper->getAlternateStoreIds();
            foreach ($stores as $storeId) {
                if ($storeId != $currentStoreId) {
                    try {
                        /** @var Mage_Core_Model_Url_Rewrite */
                        $rewrite = Mage::getModel('core/url_rewrite')
                            ->setStoreId($storeId)
                            ->loadByIdPath(sprintf('product/%d', $product->getId()));

                        if ($rewrite->getId()) {
                            $url = Mage::getUrl('', array('_direct' => $rewrite->getRequestPath(), '_nosid' => true, '_store' => $storeId));
                            $hreflang = $helper->getHreflang($storeId);
                            if ($url != $currentUrl && $hreflang) {
                                // set alternate urls on the product
                                // get it from the product like $product->getAlternateUrl(1) for store id 1
                                $alternateUrls = $product->getAlternateUrl();
                                if (!is_array($alternateUrls)) {
                                    $alternateUrls = array();
                                }
                                $alternateUrls[$storeId] = $url;
                                $product->setAlternateUrl($alternateUrls);

                                $block = Mage::app()->getLayout()->getBlock('head');
                                if ($block) {
                                    $block->addItem('link_rel', $url, 'rel="alternate" hreflang="' . htmlspecialchars($hreflang) . '"');
                                }
                            }
                        }
                    } catch (Exception $e) {
                        // ignore errors, e.g. a non existent store
                    }
                }
            }
        }
    }

    /**
     * Add rel="alternate" to category view pages
     *
     * event: controller_action_layout_render_before_catalog_category_view
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function addRelForCatalogCategoryView(Varien_Event_Observer $observer)
    {
        /** @var Mage_Catalog_Model_Category */
        $category = Mage::registry('current_category');
        if (!$category) {
            return;
        }

        $currentUrl = $category->getUrl();
        $currentStoreId = Mage::app()->getStore()->getId();
        $helper = $this->_helper();

        if ($helper->isEnabledForCategories()) {
            $stores = $helper->getAlternateStoreIds();
            foreach ($stores as $storeId) {
                if ($storeId != $currentStoreId) {
                    try {
                        /** @var Mage_Core_Model_Url_Rewrite */
                        $rewrite = Mage::getModel('core/url_rewrite')
                            ->setStoreId($storeId)
                            ->loadByIdPath(sprintf('category/%d', $category->getId()));

                        if ($rewrite->getId()) {
                            $url = Mage::getUrl('', array('_direct' => $rewrite->getRequestPath(), '_nosid' => true, '_store' => $storeId));
                            $hreflang = $helper->getHreflang($storeId);
                            if ($url != $currentUrl && $hreflang) {
                                $block = Mage::app()->getLayout()->getBlock('head');
                                if ($block) {
                                    $block->addItem('link_rel', $url, 'rel="alternate" hreflang="' . htmlspecialchars($hreflang) . '"');
                                }
                            }
                        }
                    } catch (Exception $e) {
                        // ignore errors, e.g. a non existent store
                    }
                }
            }
        }
    }


    /**
     * Get helper object
     *
     * @return Mgcs_RelAlternate_Helper_Data
     */
    protected function _helper()
    {
        return Mage::helper('mgcs_relalternate');
    }

}